import Count from "./components/Count";

function App() {
  return (
    <div>
      <Count initNumber = {20}/>
    </div>
  );
}

export default App;
