import { Component } from "react";

class Count extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count:this.props.initNumber
        }

        //C1: dung ham bind de chuyen this ve class
        //this.onButtonClickHandler = this.onButtonClickHandler.bind(this);
    }
    /*
    onButtonClickHandler() {
        console.log("Click button...");
        //this.state.count = this.state.count + 1;
        this.setState({
            count:this.state.count + 1
        })
    }*/
    //C2: dung arrow function (this sẽ thuộc về class)
    onButtonClickHandler = () => {
        console.log("Click button...");
        //this.state.count = this.state.count + 1;
        this.setState({
            count:this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.onButtonClickHandler}>Click me</button>
            </div>
        )
    }
}

export default Count